# about wilderland.ie

Project components:

- 11ty as a static site generator
- staticCMS as a git cms

## To start developing:

```
npm install
npm run dev
```

## To build only:

```
npm run build
```

### Adding polygons to leaflet via MD

Used http://polygons.openstreetmap.fr/index.py to get the GeoJSON of a relation. The Wild Nephin National Park has `753474` as a boundary relation (administrative boundary in OSM?). Then the GeoJSON was saved into the MD as a 'position' in the Map collection. 