---
layout: communities.njk
title: communities
communities_image: assets/images/wld-homepage-wild_nephin-claggan_trail-brian_wilson.jpg
communities_heading: |
  communities
communities_body: >
  Text about communities about exploring wildness in the landscape and in the
  creative space within ourselves, asking us to reconsider how we position
  ourselves in relation to the natural world, and how our daily actions can have
  far-reaching impacts on ecological systems. It is about where the wild places
  are - discovering them, protecting them, creating them.
---
Communities page