---
title: World Listening Day - Live Streaming
layout: event.njk
date: 2024-07-18T12:00:00+01:00
enddate: 2024-07-18T14:00:29+01:00
thumbnail: assets/images/wld-event-20240718-worldlisteningday-title.jpg
tags:
  - events
---
**DATE: 18th July 2024**

**TIME: 12-2pm**

**VENUE: Online Event**

**WORLD LISTENING DAY 2024 - Listening to the Weave of Time - A global community event**

#### Join sound artist Anne Marie Deacy for a two hour sonic glimpse into Wild Nephin Park.

Begining on July 18th for World Listening Day 2024 join sound artist Anne Marie Deacy for a two hour sonic glimpse into the wonderfully diverse habitat of Wild Nephin National Park, through a live streamed deep listening from 12pm to 2pm.


[Listen to the Fuaimanna Wild Nephin live stream here](https://worldlisteningday.org/events/world-listening-day-with-sound-artist-anne-marie-deacy-the-wilderland-project-at-wild-nephin-national-park-ireland/)  or by access by going to the [Locus Sonus soundmap](https://locusonus.org/soundmap/). This leads to the locus Sonus live stream map of the world, click on the microphone based on the west of Ireland called Fuaimeanna Wild Nephin and join us for two hours of deep listening celebrating the Wilderland Project and World Listening Day 2024.


[Locus Sonus Stream Project](https://locusonus.org/locustream/) is a network of open microphones that stream the captured audio environment live from locations spread around the globe.They provide the technology and support for this project and the microphones are maintained by a network of sound artists. Over the last ten years this project has developed from a single remote microphone, to become a worldwide pooled resource used by numerous artists and enjoyed by countless listeners. 


[World Listening Day](https://worldlisteningday.org/) is an annual global community event organized by the World Listening Project, that celebrates the art of actively listening to the sounds of the environment. It aims to raise awareness about the significance of listening as a cultural practice and promotes sonic stewardship of the environment. Through various events and initiatives, it encourages individuals worldwide to engage with and reflect upon the soundscape around them.


[The World Listening Project](https://www.worldlisteningproject.org/) is dedicated to understanding the world and its natural environment, societies, and cultures through the practice of listening. By promoting active listening and acoustic ecology, the World Listening Project aims to raise awareness about the importance of sound in our lives and its role in shaping our understanding of the world.


[@worldlistening](https://www.instagram.com/worldlistening)



[@forasnafuaime](https://www.instagram.com/forasnafuaime )



[@wildnephin_official](https://www.instagram.com/wildnephin_official)



[@locussonus](https://www.instagram.com/locussonus/)

