---
title: Cyanotype Workshop
layout: event.njk
date: 2024-06-23T12:00:29+01:00
enddate: 2024-06-23T14:00:29+01:00
thumbnail: assets/images/wld-event-20240623-cyanotype-title.jpg
tags:
  - events
  - workshop
  - art
---
**VENUE: Ballycroy Visitor Centre, Wild Nephin National Park, Ballycroy, Westport, Co. Mayo**

#### Join artist [Clare Breen](https://wilderland.materialsmatter.ie/artists/clare.html) for a free cyanotype sun-printing workshop and introduction to her practice and the Flower Féilire project. 

In this workshop artist Clare will introduce us to cyanotype printing, sometimes called sun-printing. It is one of the oldest photography techniques and the process works quickly in reaction to light, transforming images into beautiful blue prints. 

Clare will demonstrate the process and will guide participants step-by-step through the making of a print. We may be outside for a short while to forage materials, so please dress for the weather.

We’ll also hear about her Flower Féilire project, a year-long plant collection project to produce a collaborative cyanotype artwork for Mayo's indigenous plant life, and how you and your community can join in.

Everyone welcome.
