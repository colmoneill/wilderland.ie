---
title: Flower & Seed Collecting Walk
layout: event.njk
date: 2024-08-18T11:30:00+01:00
enddate: 2024-08-21T14:00:29+01:00
thumbnail: assets/images/wld-web-event-seedwalk-title.jpg
tags:
  - events
  - information session
---
**DATE: 18th August 2024**

**TIME: 10.30am**

**VENUE: Ballycroy Visitor Centre, Wild Nephin National Park, Ballycroy, Co. Mayo**

#### Join Wilderland for a free flower and seed collecting walk & talk with the National Parks & Wildlife Service.

We’ll learn to identify native wildflowers, their benefits for biodiversity, and how to collect and plant their seeds to create a wildflower meadow and help increase biodiversity where you live.

We will also be collecting flowers and leaves with the community for artist [Clare Breen](https://wilderland.materialsmatter.ie/artists/clare.html) and [Kasia Kaminska's](https://wilderland.materialsmatter.ie/artists/kasia.html) year-long collaborative plant collection project Flower Féilire, to produce a cyanotype tapestry for Mayo's indigenous plant life. 

Everyone welcome.
