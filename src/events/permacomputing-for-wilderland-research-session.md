---
title: Permacomputing for Wilderland - Research session
layout: event.njk
date: 2024-06-19T14:29:52+01:00
enddate: 2024-06-19T16:29:52+01:00
thumbnail: assets/images/wld-event-20240619-permacomputing-title.jpg
tags:
  - events
---
This workshop is the first of three that focus on how permacomputing principles have been applied to the Wilderland project and its digital infrastructure. Permacomputing is _“a blend of the words permaculture and computing, is a potential field of convergence between technology, cultural work, environmental research, and activism”_ [(Mansoux et al., 2023)](https://limits.pubpub.org/pub/6loh1eqi/release/1).

In this workshop, researcher Colm O'Neill will propose practical demonstrations of selected permacomputing principles that are at play in Wilderland for participants to be informed by and to later discuss. This workshop is a research session that is part of a PhD research project on the very topic of permacomputing for Wilderland, and how these ideas can help develop a better (possibly more critical) understanding of the infrastructure of everyday computing, the devices we use, and the digital networks we use.

In more detail: this project aims to assess if bringing attention to ecological and consumption harms of modern computation can generate changes of opinions regarding the generally positive outlook general populations have on technology. A number of harms and biases are already known in the area of modern computation, this project aims to highlight a lesser-known harm, namely the ecological issues surrounding contemporary and future digital technology, these it to the list of hams, and later assess if this new awareness can change people's opinions. Furthermore, the project aims to assess if ecological issues in this sphere can have an impact on digital education and digital justice.

Participants should be willing to engage in conversation and reply to short questions after session completion. If you are interested in the topics outlined above please register for the research session following [this link](etherpad.org).

For any event questions please contact Colm O'Neill ([mail@colm.be](mail@colm.be) or [oneillc3@lancaster.ac.uk](oneillc3@lancaster.ac.uk)) or Elaine Harrington (Wilderland project lead — [elaine@wilderland.ie](elaine@wilderland.ie))
