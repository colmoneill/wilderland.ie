---
title: Cyanotype Workshop
layout: event.njk
date: 2024-10-10T15:30:29+01:00
enddate: 2024-10-10T17:30:29+01:00
thumbnail: assets/images/wld-event-20241010-cyanotype-title.jpg
tags:
  - events
  - workshop
  - art
---
**VENUE: Scoil Néifinn National School Keenagh, Keenagh Beg, Ballina, Co. Mayo, F26 XR04**

#### Join artist [Kasia Kaminska](https://wilderland.materialsmatter.ie/artists/kasia.html) for a free cyanotype sun-printing workshop and introduction to her practice and the Flower Féilire project. 

In this workshop artist Kasia Kaminska will introduce us to cyanotype printing, sometimes called sun-printing. It is one of the oldest photography techniques and the process works quickly in reaction to light, transforming images into beautiful blue prints. 

Kasia will demonstrate the process and will guide participants step-by-step through the making of a print. We may be outside for a short while to forage materials, so please dress for the weather.

We’ll also hear about Wilderland’s Flower Féilire project, a year-long plant collection project to produce a collaborative cyanotype artwork for Mayo's indigenous plant life, and how you and your community can join in.

Everyone welcome.




