---
title: Papermaking workshop
layout: event.njk
date: 2024-08-21T10:00:29+01:00
enddate: 2024-08-21T12:30:29+01:00
thumbnail: assets/images/wld-web-event-papermaking-title.jpg
tags:
  - events
  - workshop
  - art
---
**VENUE: Ballycroy Visitor Centre, Wild Nephin National Park, Ballycroy, Co. Mayo**

#### Join artist [Mary Conroy](https://wilderland.materialsmatter.ie/artists/mary.html) for a free papermaking workshop and introduction to her practice. 

In this workshop we will explore the possibilities for papermaking using recycling paper, turf and locally found materials. 

The workshop will demonstrate the techniques used to make your own unique paper samples and explain how you can make your own equipment to do this at home. At the end of the workshop participants will have made a collection of unique papers to use for any art, craft or everyday projects

Everyone is welcome.
