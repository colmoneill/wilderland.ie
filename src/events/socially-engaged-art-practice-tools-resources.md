---
title: Socially Engaged Art Practice - Tools & Resources
layout: event.njk
date: 2024-08-30T10:00:12+01:00
enddate: 2024-08-30T16:00:55+01:00
thumbnail: assets/images/wld-event-sea-20240830-title.jpg
tags:
  - events
  - information session
  - workshop
---
**Venue: North Mayo Heritage Centre, Enniscoe, Castlehill, Ballina, F26FR94**

Wilderland is collaborating with Mayo Arts Service to support local artists and creative practitioners in developing sustainable and socially engaged arts practices through its professional development programme over 2024-25. 

Socially engaged art practice involves artists and communities in collaboration to create social change. This information day explores a variety of tools, materials, and services available to help artists create works that are socially responsible and locally viable.

It will be an active and engaging day of presentations, discussion, demonstrations, and practical workshops.

#### Contributors for the day include:

- Áine Crowley – Programme Manager, Arts and Engagement, Create
- Anna Livia Cullinan – Creative Communities Engagement Officer, Creative Ireland, Mayo.
- Lisa Hallinan – Arts Project and Event Manager
- Elaine Harrington – Irish artist exploring ecology of making, and Wilderland coordinator
- Sheila Coll – Mayo Community Futures

#### Topics include:

- Principles of Creative Placemaking - Workshop with Elaine Harrington
- More than materials, more than collaboration -  Discussion moderated by Lisa Hallinan
- Introduction to Inclusive Design and Community Engagement - Presentation with Anna Livia Cullinan

**Cost:** Free but booking is essential.

**Booking:** [mayoarts@mayococo.ie](mailto:mayoarts@mayococo.ie)

(Lunch will be provided. Please advise of special food requirements).
