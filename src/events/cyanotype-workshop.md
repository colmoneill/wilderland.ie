---
title: Eco Photography Workshop
layout: event.njk
date: 2024-10-09T18:00:29+01:00
enddate: 2024-10-09T19:30:29+01:00
thumbnail: assets/images/wld-event-20241009-ecophoto-title.jpg
tags:
  - events
  - workshop
  - art
---
**VENUE: Kiltane Gaa Club, Shragraddy, Bangor Erris, Ballina, Co. Mayo**

#### Join artist [Kasia Kaminska](https://wilderland.materialsmatter.ie/artists/kasia.html) **for a free eco-photography anthotype workshop and introduction to her practice and the Flower Féilire project.** 

During this workshop artist Kasia Kaminska will introduce the ​eco-photography ​Anthotype printing process, a sustainable photography process made with plants, paper and the sun. 

Kasia will give a brief introduction to the Anthotype process and its history and will guide participants step-by-step through the making of a print - from gathering materials to preparing them for printing. We may be outside for a short while to forage materials, so please dress for the weather.

We’ll also hear about the Wilderland public art and climate action project, and how you can join its collaborative art and ecology projects over the next year, starting with the Flower Féilire year-long plant collection project to produce a collaborative cyanotype artwork for Mayo's indigenous plant life.

Everyone welcome
