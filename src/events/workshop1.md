---
title: Scoping Session for local artists
layout: event.njk
date: 2024-04-12T16:15:00+01:00
enddate: 2024-04-12T17:00:30+01:00
thumbnail: assets/images/art-int-eh-landscape-promo.jpg
tags:
  - events
  - information session
---
**MAYO: Áras Inis Gluaire, Bellmullet Civic Center, Church St, Bellmullet, F26W5H0**

An invitation to join one of the first scoping sessions for artists, living and working in Mayo, who are interested in developing sustainable arts practices.

The session will include a short inroduction introduction to Wilderland and a discussion on how the project can respond to local needs.
