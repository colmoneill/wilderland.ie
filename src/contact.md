---
layout: contact.njk
title: Contact
---
For information about WILDERLAND or to get involved in the project email us on [hello@wilderland.ie](mailto:hello@wilderland.ie), or reach out to the individual artists, who share contact information on their [artist pages](/artists/).
