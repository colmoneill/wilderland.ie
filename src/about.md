---
layout: about.njk
title: homepage
intro: |
  A brief intro to Wilderland
eop_heading: |
  ecology of place
eop_body: |
  text about ecology of place
CJ_heading: |
  climate justice and the people's transition
CJ_body: |
  text about climate justice and TASC
PC_heading: |
  Permacomputing 
PC_body: >
  a blend of the words permaculture and computing, is a potential field of
  convergence between technology, cultural work, environmental research, and
  activism. [In essence permacomputing aims to promote and experiment with a
  more sustainable relationship with computer and network
  technology.](https://doi.org/10.21428/bf6fb269.6690fc2e) At a time when
  computational culture seems to be increasingly characterised by electronic and
  energy waste, permacomputing instead encourages a more sustainable approach by
  maximising the life of hardware, minimising energy consumption and focusing on
  the use of already available computing devices and components.
---
ABOUT page