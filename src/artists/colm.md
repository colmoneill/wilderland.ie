---
lastname: O'Neill
title: Colm
layout: artist.njk
practice: Digital and Media Art
bio: >
  Graphic designer and lecturer, working on digital literacy and digital
  citizenship via permacomputing concepts.
email: mail@colm.be
socialmedialinks:
  - nameoflink: Website
    onlinelink: http://colm.be
  - nameoflink: Mastodon
    onlinelink: https://post.lurk.org/@colm
photo: assets/images/variaals-_cian_flynn-46.jpg
artistimages:
  - image: assets/images/10-raw0003.jpg
    imagetitle: a landscape
  - image: assets/images/12-raw0001.jpg
    imagetitle: a boat
tags:
  - artist
  - collaborator
---
Colm O'Neill is a print and digital graphic designer who holds a BA in Visual Communications and an MA in Media Design. He is a lecturer in Design and IT at SETU since 2019, with a focus on User Interface design, Prototyping and User Experience design. Since 2022, his PhD research (Lancaster University) in Education and Social Justice involves permacomputing, a radically sustainable approach to computing.

Colm works with Materials Matter and extends its sustainable thinking by considering the digital and communication requirements of these current times. This means acknowledging that computation and the internet can have a direct, negative, impact on the climate. Materials Matter chooses to self-host its site on a recycled server, and chooses to use non-proprietary digital tools for collaboration. A similar, wider-scale approach is being applied to the WILDERLAND project.
