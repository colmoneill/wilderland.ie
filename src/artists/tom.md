---
lastname: Meskell
title: Tom
layout: artist.njk
practice: Visual artist
bio: >
  Tom is a professional artist for 30 years and over that time has developed a
  substantial catalogue of work in Ireland and abroad. Working within community
  contexts has profoundly influenced his core principals as an artist – namely
  that his work is authentic, celebratory and respectful of citizen
  participation.


  Tom’s work has allowed him to learn from the communities he has engaged with, which is continually enriching his practice. For years Tom has been exploring the capacity of translucent materials and illuminated forms as accessible materials to engage with communities to explore existential themes such as environmental awareness, citizenship,bereavement and memory.


  Tom was a founder member of “HEDGE SCHOOL“ with Jean Conway eleven years ago, this was an innovative creative exploration for artists of creating art within the natural environment.
email: ""
socialmedialinks:
  - nameoflink: Instagram
    onlinelink: https://www.instagram.com/tom_meskell/
  - nameoflink: Website
    onlinelink: http://www.tommeskell.com/
photo: assets/images/tom_meskell.jpg
tags:
  - artist
---
