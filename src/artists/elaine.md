---
lastname: Harrington
title: Elaine
layout: artist.njk
practice: Multidisciplinary artist / Wilderland project lead
bio: |
  fill in bio
email: elaine@wilderland.ie
socialmedialinks:
  - nameoflink: Instagram
    onlinelink: https://www.instagram.com/elharringtonceramics/
tags:
  - artist
---
