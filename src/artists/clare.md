---
layout: artist.njk
lastname: Breen
title: Clare
practice: Visual artist / Socially engaged practice
bio: >
  Clare is an artist and learning curator and is currently based in Carlow
  working as freelance artist-researcher and with VISUAL Centre for contemporary
  art.


  Clare works with community groups, schools and artists to develop learning programmes that build strong ties with communities inside of and beyond the walls of institutions.


  She has a degree in Fine Art from NCAD and a Master in Education in the Arts from the Piet Zwart Institute in the Netherlands. Clare is particularly interested in collaborating with artists and researchers who are curious about the dialogues that can arise when their work enters the public sphere.
email: ""
socialmedialinks:
  - nameoflink: Instagram
    onlinelink: https://www.instagram.com/clarelbreen/
  - nameoflink: Website
    onlinelink: https://clarelbreen.hotglue.me/
photo: assets/images/clare_breen-breadfellows-3.jpg
artistimages:
  - image: assets/images/clare_breen-breadfellows-1.jpg
    imagetitle: Breadfellows - Clare Breen
  - image: assets/images/clare_breen-breadfellows-2.jpg
    imagetitle: Breadfellows - Clare Breen
  - image: assets/images/clare_breen-breadfellows-4.png
    imagetitle: Workshop - Clare Breen
tags:
  - artist
---
