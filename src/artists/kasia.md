---
lastname: Kaminska
title: Kasia
layout: artist.njk
practice: Photographer / Educator
bio: >
  Kasia Kaminska is an artist and educator living in Co. Kilkenny. Her
  photographic projects and publications have been included in exhibitions (Axis
  Ballymun, TULCA,) and book fairs (Limerick International Publishers Salon,
  Dublin Art Book Fair) throughout Ireland. 


  She has been awarded artist residencies at Butler Gallery in Kilkenny, Cow House Studios in Co. Wexford and Grizedale Arts in the UK. She is co-founder of Read That Image photobook collective with whom she has co-led book design projects (shortlisted for the Unseen Dummy Award, Kassel Dummy Award and awarded the FEP European Photobook Award) and facilitated workshops in partnership with Temple Bar Gallery + Studios, Gallery of Photography, The Library Project, RHA, RCC Letterkenny, VAI, NCAD, TUDublin and Ulster University, amongst others, since 2013. 


  Her practice is led by issues of eco-social concern and she has contributed to various art and ecology projects including 'Let The Long Grass Grow' in collaboration with The Acorn Project, Kilkenny (2023-2024), 'Human Lab' at Electric Picnic curated by Aisling Murray (2022), ‘Community Vaults’ at Axis Ballymun curated by Maeve Stone (2021-2022), ‘Seeds of the Future’ at the Kilkenny School of Food (2021), and ‘How do we start?’ (2021), a climate art magazine commissioned by Project Arts Centre and curated by Maeve Stone. She holds a BA in Photography from TUDublin and an MA in Social Practice from the Limerick School of Art & Design, TUS with First Class Hons.
socialmedialinks:
  - nameoflink: Instagram - Kasia Kaminska
    onlinelink: https://www.instagram.com/kasia.kaminska/
  - nameoflink: Instagram - Read That Image
    onlinelink: https://www.instagram.com/readthatimage/
  - nameoflink: Website
    onlinelink: https://www.kasiakaminska.ie/
photo: assets/images/kasia_kaminska-profile.jpg
artistimages:
  - image: assets/images/kasia_kaminska-workshop-1.jpg
    imagetitle: Photography workshop - Kasia Kaminska
  - image: assets/images/kasia_kaminska-workshop-2.jpg
    imagetitle: Photography workshop - Kasia Kaminska
  - image: assets/images/kasia_kaminska-workshop-3.jpg
    imagetitle: Photography workshop - Kasia Kaminska
  - image: assets/images/kasia_kaminska-workshop-4.jpg
    imagetitle: Photography workshop - Kasia Kaminska
  - image: assets/images/kasia_kaminska-workshop-5.jpg
    imagetitle: Photography workshop - Kasia Kaminska
tags:
  - artist
---
