---
lastname: Ní Fhlaibhín
layout: artist.njk
title: Laura
practice: Multidisciplinary artist
bio: >
  Sifting stories, materials and traces associated with site, memory, myth,
  narratives of care and the casting of spells, Laura Ní Fhlaibhín creates
  complex but pithy material scenarios.


  These may incorporate condensed sculptural images, mineral deposits, sound, instructional texts and formal gatherings of elements that serve also as ritual artefacts. Ní Fhlaibhín’s work blends myth, personal recollection and oral histories, often stories that have been occluded from state-sanctioned narratives. Synthesising systems of support, balanced between a number of actants, is a preoccupation of her practice.


  Materials, particularly those that offer healing and nourishment, are a key actant as are mechanisms of circulation, and framing apparatuses. She attends to the material entanglements of our worlds, across species and things, and to the vibrant kinships that can emerge from such alliances, in the hope of supporting more empathic relations to emerge, from the traumas of our current times. The work frequently implies the key category of care, of both self and others, humans and animals, objects and materials.
socialmedialinks:
  - nameoflink: Instagram
    onlinelink: https://www.instagram.com/lauranifhlaibhin/
  - nameoflink: Website
    onlinelink: http://www.lauranifhlaibhin.com/
photo: assets/images/laura_ni_fhlaibhinn_profile.jpg
artistimages:
  - image: assets/images/laura_ni_fhlaibhin-painkiller-1.jpg
    imagetitle: Painkiller - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-painkiller-2.jpg
    imagetitle: Painkiller - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-banana_accelerationism.jpg
    imagetitle: Banana Accelerationism - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-wet_suction_spins_forever_installation.jpg
    imagetitle: Wet Suction Spins Forever, installation view - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-wet_suction_spins_forever-2.jpg
    imagetitle: Wet Suction Spins Forever, detail - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-bath_balm-1.jpg
    imagetitle: Bath Balm - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-bath_balm-2.jpg
    imagetitle: Bath Balm, detail - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-amulets_for_an_ecstatic_afterlife-1.jpg
    imagetitle: Amulets for an Ecstatic Afterlife - Laura N Fhlaibhín
  - image: assets/images/laura_ni_fhlaibhin-amulets_for_an_ecstatic_afterlife-2.jpg
    imagetitle: Amulets for an Ecstatic Afterlife, detail - Laura N Fhlaibhín
tags:
  - artist
---
Recent exhibitions have included Belmacz London, Pallas Projects, Dublin, Whitgift Centre Croydon, Palfrey Space, London; Burren College of Art, Ireland; Deptford X, London; Tulca Festival of Visual Arts, Galway; Newington Art Space, London; Enclave, London; Space Union, Seoul; and The Lab, Dublin among others.
