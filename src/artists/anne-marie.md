---
lastname: Deacy
title: Anne Marie
layout: artist.njk
practice: Sound artist & field recordist
bio: >+
  Anne Marie Deacy is a sound artist from Co. Galway. Her focus lies in the
  creation of work that explores natural and man worlds, real and imagined,
  present and past with an ear to the future through sound, memory, materiality
  and resonance.




email: ""
socialmedialinks:
  - nameoflink: Website
    onlinelink: https://www.annemariedeacy.com/
  - nameoflink: Instagram
    onlinelink: https://www.instagram.com/forasnafuaime/
photo: assets/images/annemarie_deacy-oscillithic-img_rachel_doolin.jpg
artistimages:
  - image: assets/images/annemarie_deacy-static_ritual.jpg
    imagetitle: Static Ritual - Anne Marie Deacy
  - image: assets/images/annemarie_deacy-musica_universalis2.jpg
    imagetitle: Static Ritual - Anne Marie Deacy
  - image: assets/images/anne-marie_deacy.jpg
    imagetitle: Anne Marie Deacy
tags:
  - artist
---
Embedded in deep research, she uses field recording and listening practices as her foundation to create work which explores our perceptions, activating our senses and encouraging new dialogues to open us up to other ways of experiencing and connecting with the world around us.

Her work is elaborated through a wide range of collaborative and sonically interactive forms and articulated through sculpture, installation, transmission art, participatory actions, sonic publication, collective experiences, composition andperformance.

Her work has been showcased locally, regionally and internationally. She has been kindly supported by The Arts Council of Ireland, Creative Ireland, Galway County & City Arts Office, Galway international Arts festival, The National Sculpture Factory, and Culture Moves Europe Mobility Fund.
