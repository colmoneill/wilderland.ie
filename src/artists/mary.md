---
lastname: Conroy
title: Mary
layout: artist.njk
practice: Cross-disciplinary artist / Socially engaged art
bio: >
  Mary Conroy is a cross-disciplinary artist who graduated with a B.Des in
  Ceramics and an MA in Social Practice and the Creative Environment from
  Limerick School of Art and Design. She works in the medium of clay, ceramics,
  and mixed media within socially engaged and collaborative practice.  






  Recent projects include: _A Magic Moving Living Thing_ (2021-2023) with the Butler Gallery, Kilkenny investigating the more-than-human-histories of the River Nore in Kilkenny through archaeology, public workshops, and collaborative events. _An Urgent Enquiry_ (2019), a residency programme in collaboration with Joanna Hopkins investigating climate change and biodiversity on the Fingal Coastline, and _What Are You Made of Folkestone?,_  at Folkstone Fringe (2021), representing Ireland through EVA International on the Creative Europe project Magic Carpets, creating a site specific tile panel and exhibition with residents using found objects and locally mined clay.
socialmedialinks:
  - nameoflink: Instagram
    onlinelink: https://www.instagram.com/@maryconroyart/
  - nameoflink: Website
    onlinelink: http://www.maryconroyart.ie
photo: assets/images/MC-profile.jpg
artistimages:
  - image: assets/images/MC-tiles.jpg
    imagetitle: "Photograph of an installation by Mary Conroy; approximatively 50
      ceramic tiles are layed on a stone floor, in a mosaic pattern. The tiles
      range in colours from red, brown and orange, all in earthen colours. Their
      surface is uneven, some showcasing pitting and dimples. "
  - image: assets/images/MC-mussels_lowres-11.jpg
    imagetitle: Photograph of an installation by Mary Conroy; the angle of the
      photograph is low and close to scattering of rocks, looking like the bank
      of a river. Among the rocks are a number of white, taller shells that have
      the texture and appearance of mussels.
  - image: assets/images/MC-sculptures-2017.jpg
    imagetitle: Photograph of an installation by Mary Conroy;
tags:
  - artist
---
text about WLD ativites
