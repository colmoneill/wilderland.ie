---
layout: home.njk
title: homepage
templateEngineOverride: njk
heroimage:
  - assets/images/wld-homepage-bg1-wild_nephin-forest-pools-gareth_mccormack.jpg
intro: >
  WILDERLAND is a 2-year programme of embedded research, public engagement &
  education activities, creative community workshops, and public artworks for
  exhibition and installation in the landscape.


  It aims to raise awareness of the biodiversity crisis and foster support for the creation of a wilderness sanctuary for biodiversity and habitat preservation and to protect our natural heritage for future generations.
team:
  - name: Elaine Harrington
    position: Project Manager
    photo: assets/images/3-raw0010.jpg
  - name: Mary Conroy
    position: Artist
    photo: assets/images/10-raw0003.jpg
  - name: Clare Breen
    position: Artist
    photo: assets/images/9-raw0004.jpg
  - name: Kasia Kaminska
    position: Artist  & Photographer
    photo: assets/images/dzsqsjq.jpg
intro_color: "#ffaf7a"
text_heading: |
  public art and community **climate action**
dynamic_section_color: "#ffffff"
creative_ireland: >
  ## Creative Climate Action fund


  WILDERLAND is a recipient of the Creative Climate Action fund, an initiative from the Creative Ireland Programme. It is funded by the Department of Tourism, Culture, Arts, Gaeltacht, Sport and Media in collaboration with the Department of the Environment, Climate and Communications.


  The fund supports creative, cultural and artistic projects that build awareness around climate change and empower citizens to make meaningful behavioural transformations.



  [Further information on the Climate Action Fund is available at this link.](https://www.gov.ie/en/publication/de5d3-climate-action-fund)
sus_arts_section_color: "#ffaf7a"
sus_arts_heading: |
  sustainable arts
sus_arts_body: >
  We live in a time where we need to change our approach to production and
  consumption. We have all become disconnected from the mass produced products
  we use in our daily lives - artists and their materials are no exception.
  These three art workers share the desire to develop and disseminate a way of
  working that not only reflects their own personal commitment to material
  sustainability within their practices, but a desire to assist others to make
  art sustainably.
homepage_strip: assets/images/wld-homepage-wild_nephin-claggan_trail-brian_wilson.jpg
---
WILDERLAND is a multi-strand, interdisciplinary public art & climate action initiative that will connect people to their local environment through a 2-year programme of public events, community workshops, and site-responsive art outcomes that seek to open our eyes anew to the wild places in our landscape & current biodiversity crisis.

It is about exploring wildness in the landscape and in the creative space within ourselves, asking us to reconsider how we position ourselves in relation to the natural world, and how our daily actions can have far-reaching impacts on ecological systems. It is about where the wild places are - discovering them, protecting them, creating them.
