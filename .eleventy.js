require('dotenv').config();

module.exports = function (eleventyConfig) {
    eleventyConfig.addNunjucksFilter('date', require('./filters/njk-dayjs-filter'))
    eleventyConfig.addPassthroughCopy("src/assets");
    eleventyConfig.addPassthroughCopy("src/admin");
    eleventyConfig.addPassthroughCopy({ "src/images": "assets/images" });

    // eleventyConfig.addCollection("events", function(collectionApi) {
    //   return collectionApi.getFilteredByGlob("events/*.md");
    // });
    // eleventyConfig.addCollection("artists", function(collectionApi) {
    //   return collectionApi.getFilteredByGlob("artists/*.md");
    // });
    const md = require("markdown-it")({
      html: true,
      linkify: true,
      typographer: true,
    });
  
    eleventyConfig.addFilter("markdown", (markdownString) =>
      md.render(markdownString)
    );

    eleventyConfig.addGlobalData("permalink", () => {
      return (data) => `${data.page.filePathStem}.${data.page.outputFileExtension}`;
    });

  
    return {
      passthroughFileCopy: true,
      markdownTemplateEngine: "njk",
      templateFormats: ["html", "njk", "md"],
      dir: {
        input: "src",
        output: "_site",
        includes: '_includes',
        layouts:  '_includes/layouts',
      },
    };
  };
